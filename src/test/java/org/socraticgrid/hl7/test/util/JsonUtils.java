package org.socraticgrid.hl7.test.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * *****************************************************************************
 *
 * Copyright 2015 Cognitive Medical Systems
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *
 *
 ******************************************************************************
 */

/**
 * Support Class for JSON
 * 
 * @author jgoodnough
 *
 */
public class JsonUtils {

  

    String ret = null;

    private static ObjectMapper objectMapper;
    private static ObjectMapper compactMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
       

        compactMapper = new ObjectMapper();
        compactMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        compactMapper.setSerializationInclusion(Include.NON_NULL);
        compactMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    }

    static public ObjectMapper getMapper() {
        return objectMapper;
    }

    public static String toJsonString(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    public static String toJsonStringCompact(Object obj) throws JsonProcessingException {
        return compactMapper.writeValueAsString(obj);
    }

}
