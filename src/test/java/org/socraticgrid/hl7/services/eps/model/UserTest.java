package org.socraticgrid.hl7.services.eps.model;

import static org.junit.Assert.*;

import org.junit.Test;
import org.socraticgrid.hl7.test.util.JsonUtils;

import com.fasterxml.jackson.core.JsonProcessingException;


public class UserTest {

	@Test
	public void testName() {
		String testUserName1 = "Test user name 1";
		String testUserName2 = "Test user name 2";
		User usr = new User();
		usr.setName(testUserName1);
		assertEquals("User name should match what was just assigned",testUserName1, usr.getName());
		usr.setName(testUserName2);
		assertEquals("User name did not change as expected",testUserName2, usr.getName());
		
	}

	@Test
	public void testUserId() {
		String testUserId1 = "Id1";
		String testUserId2 = "Id2";
		User usr = new User();
		usr.setUserId(testUserId1);
		assertEquals("User Id should match",testUserId1,usr.getUserId());
		usr.setUserId(testUserId2);
		assertEquals("User id did not change as expected",testUserId2,usr.getUserId());
	}

	@Test 
	public void testJSONSerialization() throws JsonProcessingException
	{
		String checkVal = "{\"name\":\"Jerry Goodnough\",\"supportsPublishOnDemand\":false,\"userId\":\"99\"}";
		User usr = new User();
		usr.setName("Jerry Goodnough");
		usr.setUserId("99");
		try {
			String userAsJson = JsonUtils.toJsonStringCompact(usr);
			//System.out.println("E: "+checkVal+"["+checkVal.length()+"]");
			//System.out.println("F: "+userAsJson+"["+userAsJson.length()+"]");
			assertEquals("JSON string does not matchs the expected value",checkVal,userAsJson);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw e;
		}
		
	}

}
